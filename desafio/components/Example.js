import React, { Component, Fragment } from 'react'
import { Alert, TextInput, Text, Button, SectionList } from 'react-native'

export default class Example extends Component {
    state = {
        text: ''
    }

    render() {
        const colorSections = [
            { title: 'A', data: ['Amarelo', 'Azul'] },
            { title: 'B', data: ['Beje'] },
            { title: 'C', data: ['Cinza'] },
            { title: 'R', data: ['Rosa', 'Roxo'] },
            { title: 'V', data: ['Verde', 'Vermelho', 'Vinho', 'Violeta'] },
        ]

        return (
            <Fragment>
                <TextInput
                    style={{
                        height: 40,
                        borderWidth: 1,
                        borderColor: '#000',
                        padding: 10,
                        marginTop: 20,
                        marginBottom: 20
                    }}
                    placeholder="Type here!"
                    onChangeText={text => this.setState({ text })}
                />

                <Text style={{ marginBottom: 20 }}>{this.state.text}</Text>

                <Button
                    title={'Press here!'}
                    onPress={() => {
                        const { text } = this.state
                        Alert.alert(text ? text : 'Texto Vazio!')
                    }}
                />

                <SectionList
                    sections={colorSections}
                    renderItem={({ item }) => <Text>{item}</Text>}
                    renderSectionHeader={({ section }) => <Text style={{ fontWeight: 'bold' }}>{section.title}</Text>}
                    keyExtractor={(item, index) => index}
                    style={{ maxHeight: 100, marginTop: 20 }}
                />
            </Fragment>
        )
    }
}
